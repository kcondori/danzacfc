<?php

namespace app\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;

/**
 * This is the model class for table "pasos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $cita_biblica
 * @property string|null $imagen
 * @property string|null $video
 * @property int|null $codigo_ins
 *
 * @property Aprenden[] $aprendens
 * @property Instrumentos $codigoIns
 * @property Niveles[] $colorNivels
 */
class Pasos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pasos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_ins'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['cita_biblica', 'imagen', 'video'], 'string', 'max' => 50],
            [['codigo_ins'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['codigo_ins' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'cita_biblica' => 'Cita Biblica',
            'imagen' => 'Imagen',
            'video' => 'Video',
            'codigo_ins' => 'Código Instrumento',
        ];
    }

    /**
     * Gets query for [[Aprendens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprendens()
    {
        return $this->hasMany(Aprenden::class, ['codigo_pasos' => 'codigo']);
    }

    /**
     * Gets query for [[CodigoIns]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIns()
    {
        return $this->hasOne(Instrumentos::class, ['codigo' => 'codigo_ins']);
    }

    /**
     * Gets query for [[ColorNivels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColorNivels()
    {
        return $this->hasMany(Niveles::class, ['color' => 'color_nivel'])->viaTable('aprenden', ['codigo_pasos' => 'codigo']);
    }
    
    public function getInstrumento()/*DEVOLVEMOS UNA STRING CON EL NOMBRE CORRESPONDIENTE A ESTE PASO*/
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find()->select("*")->where('codigo=' . $this->codigo_ins)
        ]);
        return $dataProvider->getModels()[0]->nombre;
    }
    public function getVideoUrl(){
        $videoPath = Yii::getAlias('@webroot/videos/' . $this->video);
        $defaultVideoUrl = Url::to("@web/videos/default.webm");

        return Url::to((file_exists($videoPath)? Url::to('@web/videos/' . $this->video):$defaultVideoUrl));
    
    }

    public function getImgUrl() {
        $imgPath = Yii::getAlias('@webroot/imagenes/' . $this->imagen);
        $defaultImgUrl = Url::to("@web/imagenes/default.jpg");

        return Url::to((file_exists($imgPath)? '@web/imagenes/' . $this->imagen:$defaultImgUrl));
    }
}
