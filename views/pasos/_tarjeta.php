<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
?>
<!-- BOTONES -->
<!-- BOTON UPDATE COMENTADO
<?= Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>-->
<!-- BOTON DE BORRAR COMENTADO 
        <?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Seguro que quieres eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>-->

<div class="tarjetas-pasos">
    <div class="card card-tarjeta-pasos" style="width: 15rem;">

    <!--COMENTADO LA IMAGEN-->
       <!-- <div class="contain-image">
            <?= Html::img(Url::to('@web/imagenes/alegria.jpg'),['class' => 'card-img-top'])?> -->
            <!-- <img class="card-img-top" src="../../web/imagenes/alegria.jpg" alt="Card image cap"> -->
       <!-- </div> -->

        <div class="card-body d-flex justify-content-center flex-column align-items-center">
            <h5 class="card-title"><?= Html::encode($model->nombre) ?></h5>
            <p class="card-text"><?= Html::encode($model->cita_biblica) ?></p>
            <?= Html::a('Aprender', ['pasos/view', 'codigo' => $model->codigo], ['class' => 'btn btn-primary'])?>
        </div>
    </div>
</div>