<?php

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/config/web.php';

(new yii\web\Application($config))->init();

$auth = Yii::$app->authManager;

// Limpia los datos anteriores
$auth->removeAll();

// Crear permisos
$createPost = $auth->createPermission('createPost');
$createPost->description = 'Crear un post';
$auth->add($createPost);

$updatePost = $auth->createPermission('updatePost');
$updatePost->description = 'Actualizar un post';
$auth->add($updatePost);

// Crear roles
$author = $auth->createRole('author');
$auth->add($author);
$auth->addChild($author, $createPost);

$admin = $auth->createRole('admin');
$auth->add($admin);
$auth->addChild($admin, $updatePost);
$auth->addChild($admin, $author);

// Asignar roles a usuarios (ejemplo)
$auth->assign($author, 2); // Asigna el rol de "author" al usuario con ID 2
$auth->assign($admin, 1);  // Asigna el rol de "admin" al usuario con ID 1

echo "RBAC inicializado correctamente.\n";

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */

