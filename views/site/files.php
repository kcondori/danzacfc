<?php

use yii\helpers\Html;
use app\models\File;

/* @var $this yii\web\View */
/* @var $files array */

$this->title = 'Archivos';
?>
<div style="display: flex; justify-content:space-around; align-items:center">
    <div class="col-md-3"></div>
    <h1><?= Html::encode($this->title) ?></h1>
    <p><?= Html::a('Subir Archivo', ['upload'], ['class' => 'btn btn-primary', 'style' => 'margin-top:0.7vh'],) ?></p>
    <div class="col-md-3"></div>
</div>
<ul class="d-flex flex-wrap justify-content-evenly" style="margin-left: 15vw;margin-right:15vw">
    <?php foreach ($files as $file) : ?>
        <div class="card card-tarjeta-pasos">
            <div class="card-body d-flex flex-column justify-content-center align-items-center" style="border: 1px solid white;border-radius: 10%; margin:2%">
                <p><?= basename($file) ?></p>

                <p><?= Html::a('Ver', ['view-file', 'filename' => basename($file)], ['class' => 'btn btn-primary']) ?></p>
                <p><?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['delete-file', 'filename' => basename($file)], [
                        'class' => 'btn btn-primary',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this file?',
                            'method' => 'post',
                        ],
                    ]) ?></p>

            </div>
        </div>
    <?php endforeach; ?>
</ul>