<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Telefonos;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Url;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Alumnos $model */

// $this->title = $model->codigo;
// \yii\web\YiiAsset::register($this);
?>
<div class="alumnos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <div class="d-flex flex-column justify-content-around align-items-center">
    <p>
        <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
        <?=User::canIn(["alumna","maestra","admin"])? Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']):"" ?>
        <?= User::can("admin")?Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Seguro que quieres eliminar esta alumna?'),
                'method' => 'post',
            ],
        ]):"" ?>
    </p>
    <div class="d-flex justify-content-center">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo',
            'nombre_y_apellidos',
            'fecha_nacimiento',
            'fecha_ingreso',
            'celula',
            'color_nivel',
        ],
        'options' => ['class' => ['custom-detail-view']]
    ]) ?>  

    <?= GridView::widget([
        'dataProvider' => $instrumentos,
        'columns' => [
            [
                'attribute' => 'nombre',
                'label' => 'Instrumentos',
            ],
        ],
        'options' => ['id'=> 'U2', 'style' => 'margin-left:2vw'],
        'summary' => ''
    ]); ?>  

    <?= GridView::widget([
        'dataProvider' => $telefonos,
        'columns' => [
            'telefono'
        ],
        'options' => ['id'=> 'U1', 'style' => 'margin-left:2vw'],
        'summary' => ''
    ]); ?>


    </div>

    </div>
    
</div>
