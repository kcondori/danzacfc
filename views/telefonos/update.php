<?php

use yii\helpers\Html;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = Yii::t('app', 'Editar Telefonos: {name}', [
    'name' => $model->id,
]);
?>
<div class="telefonos-update">

<div class="d-flex justify-content-center">
    <p style="margin-right: 1vw;margin-top: 0.7vh"><?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?></p>
    <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
        'alumno' => $alumno,
        'codigo' => $alumno
    ]) ?>

</div>
