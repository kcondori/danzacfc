<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/** @var yii\web\View $this */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="row row-carousel w-100 mx-0">
        <div class="overlay"></div>
        <div id="escueladanza" class="col text-center">
            <h1 class="tracking-in-expand"><span class="escuela-danza">ESCUELA</span> DANZA CFC</h1>
        </div>
        <div id="carouselExampleSlidesOnly" class="carousel slide w-100" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?= Html::img('@web/imagenes/imagen1home.jpg', ['class' => 'd-block w-100', 'alt' => 'First slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= Html::img('@web/imagenes/imagen2home.jpg', ['class' => 'd-block w-100', 'alt' => 'Second slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= Html::img('@web/imagenes/imagen3home.jpg', ['class' => 'd-block w-100', 'alt' => 'Third slide']) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row row-2">
        <div class="titles d-flex flex-column w-100 text-center py-4 align-items-center">
            <h3 class="sub-title-home">"Y cada golpe de la vara justiciera que asiente Jehová sobre él, será con panderos
                <br> con arpas; y en batalla tumultuosa peleará contra ellos."    Isaías 30:32</h3>
            <h2 class="titulo-home d-flex flex-column">
                INSTRUMENTOS

            </h2>
        </div>
        <?= ListView::widget([
            'dataProvider' => $instrumentos,
            'itemOptions' => ['style' => 'display: flex;'],
            'layout' => "{items}",
            'itemView' => "../instrumentos/_tarjeta",
            'options' => ['style' => 'margin-bottom:5vh;',
            'class' => 'list-view']
        ]); ?>
    </div>
</div>