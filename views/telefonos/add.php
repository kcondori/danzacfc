<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */

$this->title = Yii::t('app', 'Editar Telefonos: {name}', [
    'name' => $model->id,
]);
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_add', [
        'model' => $model,
    ]) ?>

</div>