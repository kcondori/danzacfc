<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
/** @var yii\web\View $this */
/** @var app\models\Maestros $model */

$this->title = $model->nombre_y_apellidos;

\yii\web\YiiAsset::register($this);
?>
<div class="maestros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
        <?= Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'codigo' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Seguro que quieres eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo',
            'nombre_y_apellidos',
            'fecha_nacimiento',
            'agno_graduacion',
            'celula',
            'telefono',
        ],
        'options' => ['class' => ['custom-detail-view']]
    ]) ?>

</div>
