<?php

use yii\helpers\Html;
use app\models\Telefonos;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use app\models\User;


/** @var yii\web\View $this */
/** @var app\models\Alumnos $model */
$telefono = Telefonos::from($model->codigo);

$this->title = Yii::t('app', 'Editar: {name}', [
    'name' => $model->nombre_y_apellidos,
]);

?>
<style>
div.grid-view#w1 {
    margin-left: 0;
    margin-right: 0;
}

</style>
<div class="alumnos-update">

<div class="d-flex justify-content-center">
    <p style="margin-right: 1vw;margin-top: 0.7vh"><?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?></p>
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'telefono' => $telefono
    ]) ?>
    <div class="alumnos-form2">

        <?= GridView::widget([
            'dataProvider' => $telefonos,
            'columns' => [
                'telefono',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Telefonos $model, $key, $index, $column) {
                        return Url::toRoute(['telefonos/' . $action, 'id' => $model->id]);
                    }
                ],
            ],
            'summary' => ''
        ]); ?>
        <p><?= Html::a(Yii::t('app', 'Añadir Teléfonos'), ['telefonos/createfrom', 'alumno' => $model->codigo], ['class' => 'btn btn-success']) ?>   </p>
    </div>

</div>
