<?php

/** @var yii\web\View $this */
/** @var string $content */
/* @var $this View */


use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use app\models\User;

AppAsset::register($this);
$this->registerCssFile('@web/css/site.css');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <!--<title><?= Html::encode($this->title) ?></title>-->
    <title>Danza CFC</title>
    
    <?php $this->head() ?>
    <link rel="icon" type="image/x-icon" href="<?= Yii::getAlias('@web/imagenes/faviconcon.ico') ?>">
</head>

<body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <header>
        <?php
        NavBar::begin([
            'brandLabel' => Html::img('@web/imagenes/logov1.png', ['alt' => Yii::$app->name, 'width' => '80']),
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar navbar-expand-md', // Eliminé el 'bg-dark' para personalizar el color de fondo           
            ],
        ]);
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav'],
            'items' => [
                [
                    'label' => 'Inicio',
                    'url' => ['/site/index']
                ],
                User::navItemIn(['maestra','alumna','admin'],['label' => 'Noticias', 'url' => ['/site/noticias']]),
                User::navItemIn(['alumna','maestra','admin'],['label' => 'Quiz','url' => ['/pasos/quiz']]),
                ['label' => 'Quiénes somos', 'url' => ['/maestros/somos']],
                User::navItem('admin', ['label' => 'Administración', 'url' => ['/site/administracion']]),
                User::navItemIn(['alumna','maestra','admin'],['label' => 'Materiales', 'url' => ['/pasos/index']]),
                User::navItem('maestra', ['label' => 'Mis Alumnas', 'url' => ['/alumnos/index']]),
                User::navItem('alumna', ['label' => 'Mis datos', 'url' => ['/alumnos/view', 'codigo' => Yii::$app->user->getId()]]),
                User::navItem('admin',['label' => 'Archivos', 'url' => ['/site/files']]),
                User::navItem('maestra',['label' => 'Escribir', 'url' => ['/site/noticia']]),          
                Yii::$app->user->isGuest ? 
                    ['label' => 'Contacto', 'url' => ['/site/contact']]:['label' =>'','options' => ['style' => 'display:none']],
                Yii::$app->user->isGuest ? (

                    ['label' => 'Iniciar sesión', 'url' => ['/site/login']]
                ) : (
                    '<li>'
                    . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
                    . Html::submitButton(
                         User::getNombre() . ' (Salir)',
                        ['class' => 'btn btn-link logout text-white']
                    )
                    . Html::endForm()
                    . '</li>'
                )
            ]
        ]);
        NavBar::end();
        ?>
    </header>

    <main role="main" class="flex-shrink-0">
        <div class="container">
            <?= Breadcrumbs::widget([
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>

    <footer class="footer mt-auto py-3 text-muted">
        <div class="container d-flex">
            <p class="float-left col-6">&copy; Febe Keyla Condori Villca <?= date('Y') ?></p>

            <div class="col-6 footer-social d-flex align-items-center">
                <span class="title">Síguenos en nuestras RRSS</span>
                <ul class="list-social">
                    <li class="social">
                        <?= Html::a(
                            '<i class="fa-brands fa-instagram"></i>',
                            'https://www.instagram.com/',
                            [
                                'target' => '_blank',
                                'class' => 'instagran-icon'
                            ]
                        ) ?>
                    </li>

                    <li class="social">
                        <?= Html::a(
                            '<i class="fa-brands fa-tiktok"></i>',
                            'https://www.tiktok.com/es/',
                            [
                                'target' => '_blank',
                                'class' => 'tiktok-icon'
                            ]
                        ) ?>
                    </li>


                </ul>
            </div>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>