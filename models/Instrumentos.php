<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrumentos".
 *
 * @property int $codigo
 * @property string|null $nombre
 *
 * @property Alumnos[] $codigoAlums
 * @property Pasos[] $pasos
 * @property Utilizan[] $utilizans
 */
class Instrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[CodigoAlums]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlums()
    {
        return $this->hasMany(Alumnos::class, ['codigo' => 'codigo_alum'])->viaTable('utilizan', ['codigo_ins' => 'codigo']);
    }

    /**
     * Gets query for [[Pasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPasos()
    {
        return $this->hasMany(Pasos::class, ['codigo_ins' => 'codigo']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['codigo_ins' => 'codigo']);
    }
}
