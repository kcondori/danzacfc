<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Aprenden $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="aprenden-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color_nivel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_pasos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
