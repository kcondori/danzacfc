<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Alumnos;
use app\models\Maestros;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use app\models\User;
use app\models\Instrumentos;
use app\models\Site;
use app\models\Pasos;
use app\models\Aprenden;
use app\models\Ensenan;
use app\models\Niveles;
use app\models\Utilizan;
use app\models\Telefonos;
use app\models\Noticia;
use app\models\UploadForm;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find()->select("*")
        ]);
        
        return $this->render('index',[
            "instrumentos" => $dataProvider
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        if (User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/index');
        }

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->contact($model->email)) {
                Yii::$app->session->setFlash('contactFormSubmitted');
                return $this->actionIndex();
            } else {
                return $this->actionLogin();
            }
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function publicarNoticia() {
        if (!User::can('maestra')) {
            return $this->redirect('site/login');
        }

        $model = new Noticia();
        if ($model->load(Yii::$app->request->post()) && $model->publicarNoticia()) {
            Yii::$app->session->setFlash('Noticia publicada');
            return $this->refresh();
        }
        return $this->render('publicar', [
            'model' => $model,
        ]);
    }
    
    public function actionAdministracion()
    {
        if (!User::can('admin')) {
            return $this->actionLogin();
        }
        
        $alumnos = new ActiveDataProvider(['query' => Alumnos::find()]);
        $maestros = new ActiveDataProvider(['query' => Maestros::find()]);
        $pasos = new ActiveDataProvider(['query' => Pasos::find()]);
        $aprenden = new ActiveDataProvider(['query' => Aprenden::find()]);
        $ensenan = new ActiveDataProvider(['query' => Ensenan::find()]);
        $niveles = new ActiveDataProvider(['query' => Niveles::find()]);
        $telefonos = new ActiveDataProvider(['query' => Telefonos::find()]);
        $utilizan = new ActiveDataProvider(['query' => Utilizan::find()]);
        $instrumentos = new ActiveDataProvider(['query' => Instrumentos::find()]);
        
        return $this->render('administracion',[
            'maestros' => $maestros,
            'alumnos' => $alumnos,
            'pasos' => $pasos,
            'aprenden' => $aprenden,
            'ensenan' => $ensenan,
            'niveles' => $niveles,
            'telefonos' => $telefonos,
            'utilizan' => $utilizan,
            'instrumentos' => $instrumentos
        ]);
    }

    public function actionNivelesdeaprendizaje()
    {
        return $this->render('nivelesdeaprendizaje');
    }
    public function actionMateriales()
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }
        return $this->render('materiales');
    }

    // public function actionNoticias()
    // {
    //     return $this->render('noticias', [
    //         'noticias' => Site::getNoticias()
    //     ]);
    // }

    public function actionUpload()
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->validate()) {
                $filePath = ($model->file->extension==='mp4'?'videos/':'imagenes/') .
                                 $model->file->baseName . '.' . $model->file->extension; //);
                if ($model->file->saveAs($filePath)) {
                    Yii::$app->session->setFlash('success', 'El archivo ha sido subido exitosamente.');
                } else {
                    Yii::$app->session->setFlash('error', 'Hubo un error al subir el archivo.');
                }
                return $this->refresh();
            }
        }

        return $this->render('upload', ['model' => $model]);
    }

    public function actionFiles()
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $files = array_merge(FileHelper::findFiles('imagenes'), FileHelper::findFiles('videos'));
        return $this->render('files', ['files' => $files]);
    }

    public function actionViewFile($filename)
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $filePath = Yii::getAlias('@webroot/imagenes/' . $filename);
        if (!file_exists($filePath)) {
            $filePath = Yii::getAlias('@webroot/videos/' . $filename);
            if (!file_exists($filePath)) {
                throw new NotFoundHttpException('The requested file does not exist.');
            }
        }
        return Yii::$app->response->sendFile($filePath);
    }

    public function actionDeleteFile($filename)
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $filePath = Yii::getAlias('@webroot/imagenes/' . $filename);
        if (!file_exists($filePath)) {
            $filePath = Yii::getAlias('@webroot/videos/' . $filename);
        }

        if (file_exists($filePath)) {
            unlink($filePath);
            Yii::$app->session->setFlash('success', 'The file has been deleted successfully.');
        } else {
            Yii::$app->session->setFlash('error', 'There was an error deleting the file.');
        }
        return $this->redirect(['site/files']);
    }

    public function actionLocation()
    {
        return $this->render('location');
    }

    public function actionNoticia()
    {
        if (!User::can('maestra')) {
            return $this->redirect('site/login');
        }
        
        $model = Noticia::from(User::getNombre()); // Assuming your form model is named NoticiaForm
    
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            // Create the news data
            $noticiaData = [
                'titulo' => $model->titulo,
                'autora' => $model->autora,
                'texto' => $model->texto,
                'fecha' => date('Y-m-d'),
            ];
    
            // Define the path to the JSON file
            $filePath = Yii::getAlias('@webroot/noticias/noticias.json');
    
            // Check if the file exists
            if (file_exists($filePath)) {
                // Read the existing data
                $existingData = json_decode(file_get_contents($filePath), true);
                // Check if the file is empty or not
                if (!is_array($existingData)) {
                    $existingData = [];
                }
            } else {
                // Initialize an empty array if the file does not exist
                $existingData = [];
            }
    
            // Append the new news entry
            $existingData[] = $noticiaData;
    
            // Save the updated data back to the JSON file
            file_put_contents($filePath, json_encode($existingData, JSON_PRETTY_PRINT));
    
            Yii::$app->session->setFlash('success', 'Noticia publicada exitosamente.');
            return $this->redirect(['site/noticias']);
        }
    
        return $this->render('noticia', [
            'model' => $model,
        ]);
    }
    
    public function actionUltimasnoticias()
{
    if (Yii::$app->user->isGuest) {
        return $this->redirect('site/login');
    }
    
    $filePath = Yii::getAlias('@webroot/noticias/noticias.json');
    $noticias = [];

    if (file_exists($filePath)) {
        $noticias = json_decode(file_get_contents($filePath), true);
        if (!is_array($noticias)) {
            $noticias = [];
        }
    }

    // Reverse the array to get newest first
    $noticias = array_reverse($noticias);

    // Get the latest 10 news entries
    $latestNoticias = array_slice($noticias, 0, 10);

    return $this->render('noticias', [
        'noticias' => $latestNoticias,
    ]);
}

    public function actionNoticias()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }
        
        $filePath = Yii::getAlias('@webroot/noticias/noticias.json');
        $noticias = [];

        if (file_exists($filePath)) {
            $noticias = json_decode(file_get_contents($filePath), true);
            if (!is_array($noticias)) {
                $noticias = [];
            }
        }

        // Reverse the array to get newest first
        $noticias = array_reverse($noticias);

        return $this->render('noticias', [
            'noticias' => $noticias,
        ]);
    }
}

