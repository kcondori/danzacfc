<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $id
 * @property string|null $codigo_alum
 * @property string|null $telefono
 *
 * @property Alumnos $codigoAlum
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_alum'], 'string', 'max' => 5],
            [['telefono'], 'string', 'max' => 20],
            [['codigo_alum', 'telefono'], 'unique', 'targetAttribute' => ['codigo_alum', 'telefono']],
            [['codigo_alum'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::class, 'targetAttribute' => ['codigo_alum' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_alum' => 'Código del Alumno',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[CodigoAlum]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlum()
    {
        return $this->hasOne(Alumnos::class, ['codigo' => 'codigo_alum']);
    }
    
    public function setCodigoAlum($codigo)
    {
        return $this->codigo_alum = $codigo;
    }

    public static function from($codigo) {
        $model = new Telefonos();
        $model->codigo_alum = $codigo;
        return $model;
    }
}
