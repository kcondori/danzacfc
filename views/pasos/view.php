<?php

use yii\helpers\Html;
use yii\web\View;

use yii\widgets\DetailView;
use app\controllers\PasosController;
use app\controllers\BibliaController;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Pasos $model */

$this->title = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="pasos-view">

    <!-- BOTONES -->
    <div class="row btn-fila">
        <!--TODO  que estos botones solo los puedan ver el administrador y las maestras-->

        <div class="col btn-pasos d-flex align-items-center">

            <!-- REGRESAR A PASOS -->
            <?= Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i>' .
                    Yii::t('app', 'Volver a pasos'),
                ['index'],
                ['class' => 'btn btn-primary btn-pasos']
            ) ?>

            <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
            
            <!-- ACTUALIZAR -->
            <?=  (User::canIn(['maestra','admin']))? 
                Html::a(
                '<i class="fa-regular fa-pen-to-square"></i> ' .
                    Yii::t('app', 'Actualizar'),
                ['update', 'codigo' => $model->codigo],
                ['class' => 'btn btn-primary']
            ):'' ?>

            <!-- BTN BORRAR -->
            <?= (User::canIn(['maestra','admin']))? Html::a(
                    '<i class="fa-regular fa-trash-can"></i> ' .
                        Yii::t('app', 'Borrar'),
                    ['delete', 'codigo' => $model->codigo],
                    [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::t('app', '¿Seguro que quieres borrar este elemento?'),
                            'method' => 'post',
                        ],
                    ]
                ):'' ?>
        </div>

        <!-- DESCARGAR PDF -->
        <div class="btn-right  btn-pasos">
            <?= Html::a(
                '<i class="fa-regular fa-file-pdf"></i>' .
                    "Descargar",
                ['download-pdf', 'id' => $model->codigo],
                ["class" => "btn btn-primary"]
            ) ?>
        </div>
    </div>

    <div class="row">

        <div class="col p-0 d-flex flex-column justify-content-center align-items-center">
            <h3>Patron <?= Html::encode($model->codigo) ?></h3>

            <h1 class="text-center text-uppercase titulo-home"><?= Html::encode($this->title) ?></h1>
            <?= Html::img($model->getImgUrl(), ["class" => "imagen_paso"]) ?>
        </div>
    </div>



    <div class="row jumbotron-fluid mt-5">
        <div class="card col-md-6 d-flex align-items-center"><!-- CITA BIBLICA -->
            <h3 class="text-center">
                <?= Html::encode($model->cita_biblica) ?>
            </h3>
            <p class="text-center parr-ver">
                <?= BibliaController::getVersiculo($model->cita_biblica) ?>
            </p>
            <div class="col d-flex justify-content-center">
            <video autoplay muted loop class="video-pasos">
                <source src="<?= $model->getVideoUrl() ?>" type="video/mp4">
                Tu navegador no admite el elemento <code>video</code>.

            </video>
        </div>
        </div>
        <div class="card col-md-6 d-flex align-items-center"> <!-- INSTRUMENTO -->
            <h3 class="text-center">
                <?= Html::encode($model->getInstrumento()) ?>
            </h3>

            <?= Html::img('@web/imagenes/' . str_replace(" ","_",$model->getInstrumento()) . ".jpg", ["class" => "imagen_instrumento", "style" => "width: 17vw; height: 29vw "]) ?>
        </div>
    </div>

    <div class="row mt-5">
        
    </div>


</div>