<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maestros".
 *
 * @property string $codigo
 * @property string|null $nombre_y_apellidos
 * @property string|null $fecha_nacimiento
 * @property int|null $agno_graduacion
 * @property string|null $celula
 * @property string|null $telefono
 *
 * @property Niveles[] $colorNivels
 * @property Ensenan[] $ensenans
 */
class Maestros extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maestros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['agno_graduacion'], 'integer'],
            [['codigo'], 'string', 'max' => 5],
            [['nombre_y_apellidos'], 'string', 'max' => 50],
            [['celula', 'telefono'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre_y_apellidos' => 'Nombre Y Apellidos',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'agno_graduacion' => 'Año Graduación',
            'celula' => 'Célula',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * Gets query for [[ColorNivels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColorNivels()
    {
        return $this->hasMany(Niveles::class, ['color' => 'color_nivel'])->viaTable('ensenan', ['codigo_ma' => 'codigo']);
    }

    /**
     * Gets query for [[Ensenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEnsenans()
    {
        return $this->hasMany(Ensenan::class, ['codigo_ma' => 'codigo']);
    }
    
}
