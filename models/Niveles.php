<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "niveles".
 *
 * @property string $color
 *
 * @property Alumnos[] $alumnos
 * @property Aprenden[] $aprendens
 * @property Maestros[] $codigoMas
 * @property Pasos[] $codigoPasos
 * @property Ensenan[] $ensenans
 */
class Niveles extends \yii\db\ActiveRecord
{
    public $rol;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'niveles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color'], 'required'],
            [['color'], 'string', 'max' => 15],
            [['color'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'color' => 'Color',
        ];
    }

    /**
     * Gets query for [[Alumnos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::class, ['color_nivel' => 'color']);
    }

    /**
     * Gets query for [[Aprendens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAprendens()
    {
        return $this->hasMany(Aprenden::class, ['color_nivel' => 'color']);
    }

    /**
     * Gets query for [[CodigoMas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMas()
    {
        return $this->hasMany(Maestros::class, ['codigo' => 'codigo_ma'])->viaTable('ensenan', ['color_nivel' => 'color']);
    }

    /**
     * Gets query for [[CodigoPasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPasos()
    {
        return $this->hasMany(Pasos::class, ['codigo' => 'codigo_pasos'])->viaTable('aprenden', ['color_nivel' => 'color']);
    }

    /**
     * Gets query for [[Ensenans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEnsenans()
    {
        return $this->hasMany(Ensenan::class, ['color_nivel' => 'color']);
    }
}
