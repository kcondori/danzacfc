<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ensenan".
 *
 * @property int $id
 * @property string|null $color_nivel
 * @property string|null $codigo_ma
 * @property string|null $rol
 *
 * @property Maestros $codigoMa
 * @property Niveles $colorNivel
 */
class Ensenan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ensenan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color_nivel'], 'string', 'max' => 15],
            [['codigo_ma'], 'string', 'max' => 5],
            [['rol'], 'string', 'max' => 20],
            [['color_nivel', 'codigo_ma'], 'unique', 'targetAttribute' => ['color_nivel', 'codigo_ma']],
            [['codigo_ma'], 'exist', 'skipOnError' => true, 'targetClass' => Maestros::class, 'targetAttribute' => ['codigo_ma' => 'codigo']],
            [['color_nivel'], 'exist', 'skipOnError' => true, 'targetClass' => Niveles::class, 'targetAttribute' => ['color_nivel' => 'color']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color_nivel' => 'Color Nivel',
            'codigo_ma' => 'Codigo Mestra',
            'rol' => 'Rol',
        ];
    }

    /**
     * Gets query for [[CodigoMa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMa()
    {
        return $this->hasOne(Maestros::class, ['codigo' => 'codigo_ma']);
    }

    /**
     * Gets query for [[ColorNivel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColorNivel()
    {
        return $this->hasOne(Niveles::class, ['color' => 'color_nivel']);
    }
}
