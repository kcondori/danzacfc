<?php

use app\models\Alumnos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = "Mis Alumnas";

?>
<div class="alumnos-index">
    <div class="row">
        <div class="col-md-3"></div>
        <p style="margin-right: 2vw; margin-top:0.7vh"><?= Html::a(Yii::t('app', 'Añadir Alumna'), ['create'], ['class' => 'btn btn-success']) ?></p>
        <h1><?= Html::encode($this->title) ?></h1>
        <div class=col-md-3></div>
    </div>
    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigo',
            'nombre_y_apellidos',
            'fecha_nacimiento',
            'fecha_ingreso',
            'celula',
            'telefono',
            'color_nivel',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'codigo' => $model['codigo']]);
                }
            ],
        ],
        'summary' => ''
    ]); ?>

    <?php Pjax::end(); ?>

</div>






</div>