<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Telefonos;

/** @var yii\web\View $this */
/** @var app\models\Telefonos $model */
/** @var yii\widgets\ActiveForm $form */
$model = new Telefonos(); // Crear un nuevo modelo de Telefonos
?>

<div class="telefonos-form">

    <?php $form = ActiveForm::begin([
        'id' => 'telefonos-form', // Añade un ID al formulario
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
    ]); ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?= Html::button(Yii::t('app', 'Guardar'), [
        'class' => 'btn btn-success',
        'id' => 'save-button'
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>

<?php
// Necesito que se ejecute esto al pulsar el botón Guardar

$script = <<< JS
$('#save-button').on('click', function() {
    var form = $('#telefonos-form');
    $.ajax({
        url: 'index.php?r=telefonos/save', // URL a la acción del controlador
        type: 'POST',
        data: form.serialize(),
        success: function(response) {
            if (response.success) {
                alert('Teléfono guardado correctamente.');
                // Aquí puedes añadir lógica adicional, como recargar la página o actualizar una parte de la vista.
            } else {
                alert('Error al guardar el teléfono: ' + response.errors);
            }
        },
        error: function() {
            alert('Error en la solicitud.');
        }
    });
});
JS;
$this->registerJs($script);
?>
