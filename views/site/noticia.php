<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NoticiaForm */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Escribir Noticia';
?>

<div class="site-noticia-form">
    <div class="d-flex justify-content-center" style="margin-top: 1vh">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'titulo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'texto')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'fecha')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'autora') ?>



    <div class="form-group">
        <?= Html::submitButton('Publicar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
