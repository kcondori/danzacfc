<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use yii\widgets\Pjax;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use app\models\Maestros;
use app\models\Alumnos;
use yii\helpers\Url;

/** @var yii\web\View $this */
/** @var app\models\Niveles $model */

$this->title = "Nivel: Color " . $model->color;
\yii\web\YiiAsset::register($this);
?>
<div class="niveles-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
        <?= Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'color' => $model->color], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'color' => $model->color], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Seguro que quieres eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <h2>Maestras</h2>
    <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $maestras,
            'columns' => [
                // 'codigo',
                'nombre_y_apellidos',
                'fecha_nacimiento',
                'agno_graduacion',
                'celula',
                'rol',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        // Create URLs for the CRUD actions
                        return Url::toRoute(["maestros/{$action}", 'codigo' => $model['codigo']]);
                    },
                ],
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
        <h2>Alumnas</h2>
        <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $alumnas,
        'columns' => [
            // 'codigo',
            'nombre_y_apellidos',
            'fecha_nacimiento',
            'fecha_ingreso',
            'celula',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Alumnos $model, $key, $index, $column) {
                    return Url::toRoute(["alumnos/".$action, 'codigo' => $model->codigo]);
                 }
            ],
        ],
        'summary' => '',
    ]); ?>

    <?php Pjax::end(); ?>

</div>
