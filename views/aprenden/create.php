<?php

use yii\helpers\Html;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Aprenden $model */

$this->title = Yii::t('app', 'Create Aprenden');
?>
<div class="aprenden-create">
    <div class="d-flex justify-content-center align-items-center">
    <p style="margin-right: 2vw; margin-top:1vh"><?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?><p>
    <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
