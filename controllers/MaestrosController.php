<?php

namespace app\controllers;

use app\models\Maestros;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\data\SqlDataProvider;
use app\models\User;
use Yii;
/**
 * MaestrosController implements the CRUD actions for Maestros model.
 */
class MaestrosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Maestros models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => Maestros::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Maestros model.
     * @param string $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo)
    {
        if (!User::canIn(['admin','maestra']) || (User::can('maestra') && $codigo!=Yii::$app->user->identity->id)) {
            return $this->redirect('site/login');
        }

        return $this->render('view', [
            'model' => $this->findModel($codigo),
        ]);
    }

    /**
     * Creates a new Maestros model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $model = new Maestros();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Maestros model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo)
    {
        if (!User::canIn(['admin','maestra']) || (User::can('maestra') && $codigo!=Yii::$app->user->identity->id)) {
            return $this->redirect('site/login');
        }
       
        $model = $this->findModel($codigo);
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Maestros model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo)
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }
        
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Maestros model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigo Codigo
     * @return Maestros the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo)
    {
        if (($model = Maestros::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSomos() {
        $directiva = new ActiveDataProvider([
            'query' => Maestros::find()->select("*")->where("codigo in ('15KP0','16OMC','216Z8')")
        ]);
        
        $otras = new SqlDataProvider([
            'sql' => "SELECT m.codigo, e.rol , m.nombre_y_apellidos, e.color_nivel
            FROM ensenan e INNER JOIN maestros m ON (e.codigo_ma=m.codigo)
            WHERE e.codigo_ma NOT IN ('15KP0','16OMC','216Z8')
          AND e.rol NOT IN ('Descanso','Alumna')"
        ]);
        return $this->render('somos',[
            "directiva" => $directiva->getModels(),
            "otras" => $otras
        ]);
    }
    public static function getImagen($código){
        $imagenes = ["X459W","XF549","ZUW89","361P2","ECKY8","GUIIB"]; 
        return in_array($código, $imagenes) ? Url::to("@web/imagenes/" . $código . ".jpg") : Url::to("@web/imagenes/ZUW89.jpg");
    }
    
    
    
    
}
