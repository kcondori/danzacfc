<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Ensenan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="ensenan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'color_nivel')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_ma')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rol')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
