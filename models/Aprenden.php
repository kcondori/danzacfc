<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "aprenden".
 *
 * @property int $id
 * @property string|null $color_nivel
 * @property int|null $codigo_pasos
 *
 * @property Pasos $codigoPasos
 * @property Niveles $colorNivel
 */
class Aprenden extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'aprenden';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_pasos'], 'integer'],
            [['color_nivel'], 'string', 'max' => 15],
            [['color_nivel', 'codigo_pasos'], 'unique', 'targetAttribute' => ['color_nivel', 'codigo_pasos']],
            [['color_nivel'], 'exist', 'skipOnError' => true, 'targetClass' => Niveles::class, 'targetAttribute' => ['color_nivel' => 'color']],
            [['codigo_pasos'], 'exist', 'skipOnError' => true, 'targetClass' => Pasos::class, 'targetAttribute' => ['codigo_pasos' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color_nivel' => 'Color Nivel',
            'codigo_pasos' => 'Codigo Pasos',
        ];
    }

    /**
     * Gets query for [[CodigoPasos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPasos()
    {
        return $this->hasOne(Pasos::class, ['codigo' => 'codigo_pasos']);
    }

    /**
     * Gets query for [[ColorNivel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColorNivel()
    {
        return $this->hasOne(Niveles::class, ['color' => 'color_nivel']);
    }
}
