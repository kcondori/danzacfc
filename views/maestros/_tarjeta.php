<?php

use app\controllers\MaestrosController;
use yii\helpers\Html;
?>



<div class="container">
    <div class="card">
        <?= Html::img(MaestrosController::getImagen($model["codigo"])) ?>
        <div class="overlay">
            <div class="hover-footer">
                <h5><?= $model["nombre_y_apellidos"] ?></h5>
                <h5>Nivel: <?= $model["color_nivel"] ?></h5>
                <h5><?= $model["rol"] ?></h5>
            </div>
        </div>
    </div>

</div>