<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\User;
?>


<!-- <a href=<?= Url::to(['instrumentos/view', 'codigo' => $model->codigo]) ?>>
<div class="card card-instrumentos" >
    <?= Html::img('@web/imagenes/' . str_replace(" ", "_", $model->nombre)  . '.jpg', ['alt' => 'Imagen de Prueba']) ?>
    <div class="overlay">
        <div class="icon">
            <i class="fa-solid fa-arrow-right-long"></i>
        </div>
        <div class="hover-footer">
            <h3><?=$model->nombre?></h3>
        </div>
    </div>
</div>
</a> -->
<?= Html::a(
    Html::tag('div', // Encerramos el contenido en un div
        Html::img('@web/imagenes/' . str_replace(" ", "_", $model->nombre)  . '.jpg', ['alt' => 'Imagen de Prueba']) .
        '<div class="overlay">' .
            '<div class="icon">' .
                '<i class="fa-solid fa-arrow-right-long"></i>' .
            '</div>' .
            '<div class="hover-footer">' .
                '<h3>' . $model->nombre . '</h3>' .
            '</div>' .
        '</div>'
    ),
    User::linkIn(['maestra','alumna','admin'],['instrumentos/view', 'codigo' => $model->codigo],['site/login']),
    ['class' => 'card card-instrumentos']
) ?>