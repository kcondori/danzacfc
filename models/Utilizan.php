<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $id
 * @property string|null $codigo_alum
 * @property int|null $codigo_ins
 *
 * @property Alumnos $codigoAlum
 * @property Instrumentos $codigoIns
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_ins'], 'integer'],
            [['codigo_alum'], 'string', 'max' => 5],
            [['codigo_alum', 'codigo_ins'], 'unique', 'targetAttribute' => ['codigo_alum', 'codigo_ins']],
            [['codigo_alum'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::class, 'targetAttribute' => ['codigo_alum' => 'codigo']],
            [['codigo_ins'], 'exist', 'skipOnError' => true, 'targetClass' => Instrumentos::class, 'targetAttribute' => ['codigo_ins' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_alum' => 'Codigo Alumna',
            'codigo_ins' => 'Codigo Instrumento',
        ];
    }

    /**
     * Gets query for [[CodigoAlum]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAlum()
    {
        return $this->hasOne(Alumnos::class, ['codigo' => 'codigo_alum']);
    }

    /**
     * Gets query for [[CodigoIns]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIns()
    {
        return $this->hasOne(Instrumentos::class, ['codigo' => 'codigo_ins']);
    }
}
