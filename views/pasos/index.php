<?php

use app\controllers\InstrumentosController;
use app\models\Pasos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */
use app\models\User;

$this->title = Yii::t('app', 'Pasos');
?>
<div class="pasos-index">
<!--COMENTADO TITLE
    
-->

    <div class="d-flex justify-content-center">
    <p style="margin-right: 2vw; margin-top: 0.7vh">
        <?= User::can("maestra")?Html::a(Yii::t('app', 'Añadir Paso'), ['create'], ['class' => 'btn btn-success']):"" ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    </div>
    

    

    <?php foreach ($pasos as $instrumento): ?>
        <?php Pjax::begin(); ?>
        <h2 class="mb-4 mt-5"><?=InstrumentosController::getNombre($instrumento->getModels()[0]->codigo_ins)?></h2>
    <?= ListView::widget([
        'dataProvider' => $instrumento,
        // 'itemOptions' => ['style' => 'display: flex;'],
        'layout' => "{items}",
        'itemView' => "_tarjeta",
        'options' => [
            'class' => 'd-flex card-prueba justify-content-center flex-wrap',
        ],
    ]); ?>

    <?php Pjax::end(); ?>
    <?php endforeach; ?>
    <?= Html::img($url)?>
</div>
