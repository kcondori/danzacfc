<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\widgets\ListView;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Instrumentos $model */

$this->title = $model->nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="instrumentos-view">



    <p>
        <?= User::can('admin') ? Html::a(
            '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                Yii::t('app', 'Volver a administración'),
            ['site/administracion'],
            ['class' => 'btn btn-primary btn-pasos']
        ) : '' ?>
        <?= User::can("maestra") ? Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'codigo' => $model->codigo], ['class' => 'btn btn-primary']) : "" ?>



        <!-- <?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'codigo' => $model->codigo], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::t('app', '¿Seguro que quieres eliminar este elemento?'),
                        'method' => 'post',
                    ],
                ]) ?> -->
    </p>

    <!-- <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'codigo',
                    'nombre',
                ],
                'options' => ['class' => ['custom-detail-view']]
            ]) ?> -->

    <?php Pjax::begin(); ?>
    <div class="d-flex justify-content-center">
        <h2 class="mb-4 mt-5"><?= $model->nombre ?></h2>
    </div>

    <?= ListView::widget([
        'dataProvider' => $pasos,
        // 'itemOptions' => ['style' => 'display: flex;'],
        'layout' => "{items}",
        'itemView' => "../pasos/_tarjeta",
        'options' => [
            'class' => 'd-flex card-prueba justify-content-center flex-wrap',
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>