<?php

use yii\helpers\Html;
use app\models\User;

/* @var $this yii\web\View */
/* @var $noticias array */

$this->title = 'Noticias';

?>

<div class="site-todas-las-noticias">
    <div class="d-flex justify-content-center">
    <h1><?= Html::encode($this->title) ?></h1>
    </div>

    <div class="news-list">
        
        <?php foreach ($noticias as $noticia): ?>
            <div class="news-item card-prueba tarjetas-pasos" style="border: 1px solid white; border-radius: 0.5vw; margin-bottom:1vh; text-align: justify">
                
                <h4><?= Html::encode($noticia['titulo']) ?></h4>
                <h5><?= Html::encode($noticia['autora'])?></h5>            
                <p><?= Html::encode($noticia['texto']) ?></p>
                <p style="text-align: end">Fecha de publicación: <?=Html::encode($noticia['fecha'])?></p>
            </div>
        <?php endforeach; ?>
        </div>
</div>
