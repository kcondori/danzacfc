<?php

namespace app\controllers;

use app\models\Alumnos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use app\models\User;
use app\models\Telefonos;
use yii\data\SqlDataProvider;

/**
 * AlumnosController implements the CRUD actions for Alumnos model.
 */
class AlumnosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Alumnos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        }
        
        $alumnos = new SqlDataProvider([
            'sql' => 'SELECT a.*, t.telefono FROM alumnos a INNER JOIN telefonos t ON a.codigo = t.codigo_alum
            INNER JOIN niveles n ON a.color_nivel = n.color
            INNER JOIN ensenan e ON n.color = e.color_nivel
            WHERE e.codigo_ma="' . Yii::$app->user->identity->id . '"
            GROUP BY a.nombre_y_apellidos'
        ]);


        $dataProvider = new ActiveDataProvider([
            'query' => Alumnos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $alumnos,
        ]);
    }

    /**
     * Displays a single Alumnos model.
     * @param string $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }
        if (User::can('alumna') && $codigo!=Yii::$app->user->identity->id) {
            return $this->redirect('site/index');
        }

        
        $telefonos = new ActiveDataProvider([
           'query' => Telefonos::find()->select("telefono")->where("codigo_alum='" . $codigo . "'") 
        ]);

        $instrumentos = new SqlDataProvider([
            'sql' => 'SELECT i.nombre
            FROM instrumentos i INNER JOIN utilizan u ON i.codigo = u.codigo_ins
            WHERE u.codigo_alum="' . $codigo . '"',
            'sort' => [
                    'attributes' => [
                        'nombre'
                    ]],
        ]);
        
        return $this->render('view', [
            'model' => $this->findModel($codigo),
            'telefonos' => $telefonos,
            'instrumentos' => $instrumentos
        ]);
        
    }

    /**
     * Creates a new Alumnos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        }
        
        $model = new Alumnos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Alumnos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('site/login');
        }
        if (User::can('alumna') && $codigo!=Yii::$app->user->identity->id) {
            return $this->redirect('site/index');
        }

        $telefonoModel = Telefonos::from($codigo);
        $telefonos = new ActiveDataProvider([
            'query' => Telefonos::find()->select("*")->where("codigo_alum='" . $codigo . "'")
        ]);
        $model = $this->findModel($codigo);
        
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }
    
        return $this->render('update', [
            'model' => $model,
            'telefonoModel' => $telefonoModel,
            'telefonos' => $telefonos
        ]);
    }

    /**
     * Deletes an existing Alumnos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo)
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        } 
        
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Alumnos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $codigo Codigo
     * @return Alumnos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo)
    {
        if (($model = Alumnos::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
