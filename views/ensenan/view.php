<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/** @var yii\web\View $this */
/** @var app\models\Ensenan $model */

$this->title = "Relación Enseñan: ID " . $model->id;
\yii\web\YiiAsset::register($this);
?>
<div class="ensenan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
        <?= Html::a(Yii::t('app', '<i class="fa-regular fa-pen-to-square"></i> ' . Yii::t('app', 'Actualizar')), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa-regular fa-trash-can"></i> ' . Yii::t('app', 'Eliminar'), ['update', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Seguro que quieres eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'color_nivel',
            'codigo_ma',
            'rol',
        ],
        'options' => ['class' => ['custom-detail-view']]
    ]) ?>

</div>
