<?php

use app\models\Niveles;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\User;
/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Niveles');
?>
<div class="body">
<div class="niveles-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?= User::can('admin')?Html::a(
                '<i class="fa-solid fa-arrow-right-to-bracket"></i> ' .
                    Yii::t('app', 'Volver a administración'),
                ['site/administracion'],
                ['class' => 'btn btn-primary btn-pasos']
            ):'' ?>
        <?= Html::a(Yii::t('app', 'Create Niveles'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'color',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Niveles $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'color' => $model->color]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
</div>