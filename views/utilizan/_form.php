<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Utilizan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="utilizan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_alum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_ins')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
