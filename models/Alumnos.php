<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property string $codigo
 * @property string|null $nombre_y_apellidos
 * @property string|null $fecha_nacimiento
 * @property string|null $fecha_ingreso
 * @property string|null $celula
 * @property string|null $color_nivel
 *
 * @property Instrumentos[] $codigoIns
 * @property Niveles $colorNivel
 * @property Telefonos[] $telefonos
 * @property Utilizan[] $utilizans
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['fecha_nacimiento', 'fecha_ingreso'], 'safe'],
            [['codigo'], 'string', 'max' => 5],
            [['nombre_y_apellidos'], 'string', 'max' => 50],
            [['celula'], 'string', 'max' => 20],
            [['color_nivel'], 'string', 'max' => 15],
            [['codigo'], 'unique'],
            [['color_nivel'], 'exist', 'skipOnError' => true, 'targetClass' => Niveles::class, 'targetAttribute' => ['color_nivel' => 'color']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre_y_apellidos' => 'Nombre Y Apellidos',
            'fecha_nacimiento' => 'Fecha Nacimiento',
            'fecha_ingreso' => 'Fecha Ingreso',
            'celula' => 'Celula',
            'color_nivel' => 'Color Nivel',
        ];
    }

    /**
     * Gets query for [[CodigoIns]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoIns()
    {
        return $this->hasMany(Instrumentos::class, ['codigo' => 'codigo_ins'])->viaTable('utilizan', ['codigo_alum' => 'codigo']);
    }

    /**
     * Gets query for [[ColorNivel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getColorNivel()
    {
        return $this->hasOne(Niveles::class, ['color' => 'color_nivel']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::class, ['codigo_alum' => 'codigo']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::class, ['codigo_alum' => 'codigo']);
    }
}
