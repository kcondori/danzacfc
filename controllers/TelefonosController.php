<?php

namespace app\controllers;

use app\models\Telefonos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use Yii;
use app\models\User;

/**
 * TelefonosController implements the CRUD actions for Telefonos model.
 */
class TelefonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Telefonos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!User::can('admin')) {
            return $this->redirect('site/login');
        }

        
        $dataProvider = new ActiveDataProvider([
            'query' => Telefonos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telefonos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (!User::canIn(['admin'])) {
            return $this->redirect('site/login');
        }

        
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Telefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }

        
        $model = new Telefonos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'alumno' => false,
            'codigo' => null,
        ]);
    }

    public function actionCreatefrom($alumno)
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }

        
        $model = new Telefonos();

        $model->codigo_alum = $alumno;

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'codigo' => $alumno,
            'alumno' => true
        ]);
    }

    /**
     * Updates an existing Telefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }
        
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return User::can(['alumna'])?
                $this->redirect(['/alumnos/view', 'codigo' => Yii::$app->user->getId()]):
                $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'alumno' => false,
            'codigo' => null,
        ]);
    }
    
    public function actionSavetelefono($codigo_alum,$telefono)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Telefonos();
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
        } else {
            return ['success' => false, 'errors' => $model->errors];
        }
    }

    /**
     * Deletes an existing Telefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }
        
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Telefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Telefonos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionSave()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new Telefonos();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->db->createCommand()->insert('telefonos', [
                'codigo_alum' => Yii::$app->request->post('codigo_alum'), // Ajusta esto según tu lógica
                'telefono' => $model->telefono,
            ])->execute();

            return ['success' => true];
        } else {
            return ['success' => false, 'errors' => $model->errors];
        }
    }
}
