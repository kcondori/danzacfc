
<!-- Include stylesheet -->
<link href="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.snow.css" rel="stylesheet" />

<!-- Create the editor container -->

<form>
<p>Nombre</p>
<input id="titulo" type="text">
<div id="editor">
  <p>Hello World!</p>
  <p>Some initial <strong>bold</strong> text</p>
  <p><br /></p>
</div>

<input id="publicar" type="submit">
</form>
<!-- Include the Quill library -->
<script src="https://cdn.jsdelivr.net/npm/quill@2.0.2/dist/quill.js"></script>

<!-- Initialize Quill editor -->
<script>
  const quill = new Quill('#editor', {
    theme: 'snow'
  });

  let noticia;

  document.getElementById("publicar")
    .addEventListener("click", (e) => {
        noticia = {
            titulo: document.getElementById("titulo").innerHtml,
            texto: document.getElementById("editor").innerHtml
        }
    });
    
</script>
