<?php

namespace app\controllers;

use app\models\Instrumentos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Pasos;
use app\models\User;
use Yii;

/**
 * InstrumentosController implements the CRUD actions for Instrumentos model.
 */
class InstrumentosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Instrumentos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }
        
        $dataProvider = new ActiveDataProvider([
            'query' => Instrumentos::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'codigo' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instrumentos model.
     * @param int $codigo Codigo
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($codigo)
    {
        if (!User::canIn(['admin','maestra','alumna'])) {
            return $this->redirect('site/login');
        }
        
        $pasos = new ActiveDataProvider([
            'query' => Pasos::find()->select("*")->where("codigo_ins='" . $codigo ."'")
        ]);
        return $this->render('view', [
            'model' => $this->findModel($codigo),
            'pasos' => $pasos
        ]);
    }

    /**
     * Creates a new Instrumentos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        }
        
        $model = new Instrumentos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'codigo' => $model->codigo]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Instrumentos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $codigo Codigo
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($codigo)
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        }

        $model = $this->findModel($codigo);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'codigo' => $model->codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Instrumentos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $codigo Codigo
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($codigo)
    {
        if (!User::canIn(['admin','maestra'])) {
            return $this->redirect('site/login');
        }
        
        $this->findModel($codigo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrumentos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $codigo Codigo
     * @return Instrumentos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($codigo)
    {
        if (($model = Instrumentos::findOne(['codigo' => $codigo])) !== null) {
            return $model;
        }

        // throw new NotFoundHttpException(Yii::('app', 'The requested page does not exist.'));
    }

    public static function getNombre($codigo) {
        return Instrumentos::findOne(['codigo' => $codigo])->nombre;
    }
}
