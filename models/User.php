<?php

namespace app\models;

use app\controllers\SiteController;
use Yii;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $rol;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'Saul&Keyla2808',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
            'rol' => 'admin',
        ],
        '15KP0' => [
            'id' => '15KP0',
            'username' => 'maestra',
            'password' => 'K.e.Y&1I94',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
            'rol' => 'maestra',
        ],
        '01SE7' => [
            'id' => '01SE7',
            'username' => 'alumna',
            'password' => 'aL.umn0!',
            'authKey' => 'test102key',
            'accessToken' => '102-token',
            'rol' => 'alumna',
        ],
    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
    
    /**Comprueba si el usuario actual tiene el rol adecuado para acceder a esto
     * 
     * @param type $rol
     * @return boolean
     */
    public static function can($rol){
        return Yii::$app->user->isGuest?
            false:Yii::$app->user->identity->rol === $rol;
    }
    
    /**Comprueba si el usuario actual tiene el rol dentro de los roles
     * adecuados para acceder a esto
     * 
     * @param type $rol
     * @return boolean
     */
    public static function canIn($roles) {
        foreach($roles as $rol) {
            if (User::can($rol)) {return true;}
        }
        return false;
    }
    /** Comprueba si el usuario actual puede ver el item particular del
     * navbar, comprobando si su rol coincide con el adecuado
     * 
     * @param type $rol
     * @param type $item
     * @return type
     */
    public static function navItem($rol,$item) {
        return User::can($rol)?$item:['label'=>'','options' => ['style' => 'display:none']];
    }

    /** Comprueba si el usuario actual puede ver el item particular del
     * navbar, comprobando si su rol está dentro de los adecuados.
     * 
     * @param type $rol
     * @param type $item
     * @return type
     */    
    public static function navItemIn($roles, $item) {
        foreach($roles as $rol) {
            if (User::can($rol)) {return $item;}
        }
        return ['label' =>'','options' => ['style' => 'display:none']];
    }

    public static function linkIn($roles,$linkOk,$linkErr) {
        return User::canIn($roles)?$linkOk:$linkErr;
    }

    public static function getNombre() {
        switch(Yii::$app->user->identity->rol) {
            case "admin": 
                return Yii::$app->user->identity->username;
            case "maestra":
                return Maestros::findOne(Yii::$app->user->identity->id)->nombre_y_apellidos;
            case "alumna":
                return Alumnos::findOne(Yii::$app->user->identity->id)->nombre_y_apellidos; 
        }
    }
}
