<?php
use app\models\Maestros;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Alumnos;
use app\models\Telefonos;
use app\models\Utilizan;
use app\models\Ensenan;
use app\models\Niveles;
use app\models\Pasos;
use app\models\Aprenden;
use app\models\Instrumentos;

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<div class="rows centrar">
<button class="btn btn-primary boton">Alumnas</button>
<button class="btn btn-primary boton">Maestras</button>
<button class="btn btn-primary boton">Pasos</button>
<button class="btn btn-primary boton">Aprenden</button>
<button class="btn btn-primary boton">Enseñan</button>
<button class="btn btn-primary boton">Niveles</button>
<button class="btn btn-primary boton">Teléfonos</button>
<button class="btn btn-primary boton">Utilizan</button>
<button class="btn btn-primary boton">Instrumentos</button>
</div>
    <div id="alumnas-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Alumno'), ['alumnos/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Alumnas</h1>
        <div class="col-md-4"></div>
    </div>
    

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $alumnos,
        'columns' => [
            // 'codigo',
            'nombre_y_apellidos',
            'fecha_nacimiento',
            'fecha_ingreso',
            'celula',
            'color_nivel',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Alumnos $model, $key, $index, $column) {
                    return Url::toRoute(["alumnos/".$action, 'codigo' => $model->codigo]);
                 }
            ],
        ],
        'summary' => '',
    ]); ?>

    <?php Pjax::end(); ?>

    </div>
    <div id="maestras-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Maestra'), ['maestros/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Maestras</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $maestros,
            'columns' => [
                // 'codigo',
                'nombre_y_apellidos',
                'fecha_nacimiento',
                'agno_graduacion',
                'celula',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Maestros $model, $key, $index, $column) {
                        return Url::toRoute(["maestros/".$action, 'codigo' => $model->codigo]);
                    }
                ],
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="pasos-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Paso'), ['pasos/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Pasos</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $pasos,
            'columns' => [
                // 'codigo',
                'nombre',
                'cita_biblica',
                'imagen',
                'video',
                'codigo_ins',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Pasos $model, $key, $index, $column) {
                        return Url::toRoute(["pasos/".$action, 'codigo' => $model->codigo]);
                    }
                ],
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="aprenden-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Aprenden'), ['aprenden/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Aprenden</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $aprenden,
            'columns' => [
                // 'id',
                'color_nivel',
                'codigo_pasos',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Aprenden $model, $key, $index, $column) {
                        return Url::toRoute(["aprenden/".$action, 'id' => $model->id]);
                    }
                ],
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="ensenan-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Enseñan'), ['ensenan/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Enseñan</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $ensenan,
            'columns' => [
                // 'id',
                'color_nivel',
                'codigo_ma',
                'rol',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Ensenan $model, $key, $index, $column) {
                        return Url::toRoute(["ensenan/".$action, 'id' => $model->id]);
                    }
                ],
                
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="niveles-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Nivel'), ['niveles/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Niveles</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $niveles,
            'columns' => [
                'color',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Niveles $model, $key, $index, $column) {
                        return Url::toRoute(["niveles/".$action, 'color' => $model->color]);
                    }
                ],
                
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="telefonos-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Teléfono'), ['telefonos/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Teléfonos</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $telefonos,
            'columns' => [
                'codigo_alum',
                'telefono',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Telefonos $model, $key, $index, $column) {
                        return Url::toRoute(["telefonos/".$action, 'id' => $model->id]);
                    }
                ],
                
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="utilizan-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Utilizan'), ['utilizan/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Utilizan</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $utilizan,
            'columns' => [
                'codigo_alum',
                'codigo_ins',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Utilizan $model, $key, $index, $column) {
                        return Url::toRoute(["utilizan/".$action, 'id' => $model->id]);
                    }
                ],
                
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
    <div id="instrumentos-tab" class="tab">
    <div class="d-flex justify-content-center">
        <div class="col-md-4"></div>
        <p class="col-md-1">
            <?= Html::a(Yii::t('app', 'Añadir Instrumento'), ['instrumentos/create'], ['class' => 'btn btn-success']) ?>
        </p>
        <!-- <div class="col-md-1"></div> -->
        <h1 class="col-md-1">Instrumentos</h1>
        <div class="col-md-4"></div>
    </div>
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'dataProvider' => $instrumentos,
            'columns' => [
                // 'codigo',
                'nombre',
                [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, Instrumentos $model, $key, $index, $column) {
                        return Url::toRoute(["instrumentos/".$action, 'codigo' => $model->codigo]);
                    }
                ],
                
            ],
            'summary' => '',
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
<script>
        // Recuperamos todos los botones y todas las pestañas
        const tabs = document.getElementsByClassName("tab");
        const buttons = document.getElementsByClassName("boton");

        // Al principio sólo mostramos la primera tabla
        Array.from(tabs).forEach((tab, index) => {
            if (index !== 0) {
                tab.style.display = "none";
            } else {
                tab.style.display = "block";
            }
        });

        // Añadimos la funcionalidad a los botones para que hagan que se muestre la pestaña correspondiente
        Array.from(buttons).forEach((button, index) => {
            button.addEventListener("click", () => {
                Array.from(tabs).forEach((tab, tabIndex) => {
                    tab.style.display = tabIndex === index ? "block" : "none";
                });
            });
        });
</script>

