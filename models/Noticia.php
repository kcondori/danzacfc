<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Noticia extends Model
{
    public $titulo;
    public $autora;
    public $texto;
    public $fecha;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['titulo', 'autora', 'texto'], 'required'],
            ['fecha', 'default', 'value' => date('Y-m-d H:i')],
        ];
    }

    public static function from($autora) {
        $model = new Noticia();
        $model->autora = $autora;
        return $model;
    }
}
